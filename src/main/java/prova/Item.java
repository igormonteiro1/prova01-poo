package prova;


public class Item {
	private String title;
	private String publisher;
	private int yearPublisher;
	private String ISBN;
	private double price;
	
	public Item(String title, String publisher, int yearPublisher, String ISBN, double price) {
		super();
		this.title = title;
		this.publisher = publisher;
		this.yearPublisher = yearPublisher;
		this.ISBN = ISBN;
		this.price = price;
	}

	public void display() {
		System.out.println("Titulo: "+this.title+"\nEditora: "+this.publisher+"\nData: "+this.yearPublisher+"\nISBN: "+this.ISBN+"\nPre�o: "+this.price);
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public int getYearPublisher() {
		return yearPublisher;
	}
	public void setYearPublisher(int yearPublisher) {
		this.yearPublisher = yearPublisher;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	
}
