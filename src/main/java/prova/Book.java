package prova;


public class Book extends Item {
	
	private String author;
	private String edition;
	private String volume;
	
	public Book(String title, String publisher, int yearPublisher, String ISBN, double price, String author, String edition, String volume) {
		super(title, publisher, yearPublisher, ISBN, price);
		this.author=author;
		this.edition=edition;
		this.volume=volume;
	}
	
	public void display() {
		System.out.println("Titulo: "+getTitle()+"\nEditora: "+getPublisher()+"\nData: "+getYearPublisher()+"\nISBN: "+getISBN()+"\nPre�o: "+getPrice()+"\nAutor: "+this.author+"\nEdi��o: "+this.edition+"\nVolume:"+this.volume);
	}
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
}
